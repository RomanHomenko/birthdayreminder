//
//  Person.swift
//  BirthdayReminder
//
//  Created by roman on 8/2/21.
//  Copyright © 2021 roman. All rights reserved.
//

import Foundation

struct Person {
    var name: String
    var dateToBirth: String
    
    init(name: String, dateToBirth: String) {
        self.name = name
        self.dateToBirth = dateToBirth
    }
}
