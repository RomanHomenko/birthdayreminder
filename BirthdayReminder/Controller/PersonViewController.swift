//
//  PersonViewController.swift
//  BirthdayReminder
//
//  Created by roman on 7/31/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class PersonViewController: UIViewController {

    var person = Person(name: "", dateToBirth: "")
    
    @IBOutlet weak var personImageView: UIImageView!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var dateTF: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    @IBOutlet weak var sexTF: UITextField!
    @IBOutlet weak var instagramTF: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.personImageView.image = UIImage(named: "person.jpg")
    }

    private func updateSaveButtonState() {
        let existNameText = nameTF.text ?? ""
        let existDateText = dateTF.text ?? ""
        let existAgeText = ageTF.text ?? ""
        let sexExistText = sexTF.text ?? ""
        
        saveButton.isEnabled = !existNameText.isEmpty && !existDateText.isEmpty && !existAgeText.isEmpty && !sexExistText.isEmpty
    }
    
    @IBAction func changePhotoButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
        updateSaveButtonState()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard segue.identifier == "saveSegue" else { return }
        
        let personName = nameTF.text ?? ""
        let personDate = dateTF.text ?? ""
        
        self.person = Person(name: personName, dateToBirth: personDate)
    }
}
