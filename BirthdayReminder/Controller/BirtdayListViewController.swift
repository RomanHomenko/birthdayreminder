//
//  BirtdayListViewController.swift
//  BirthdayReminder
//
//  Created by roman on 7/31/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class BirtdayListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - tableView from storyBoard
    
    @IBOutlet weak var birtdaysTableView: UITableView!
    
    var birthdays = [Person(name: "Roman", dateToBirth: "2020")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.birtdaysTableView.delegate = self
        self.birtdaysTableView.dataSource = self
    }
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {
        guard segue.identifier == "saveSegue" else { return }
        
        let birtdayVC = segue.source as! PersonViewController
        let person = birtdayVC.person
        
        let newIndexPath = IndexPath(row: birthdays.count, section: 0)
        birthdays.append(person)
        self.birtdaysTableView.insertRows(at: [newIndexPath], with: .fade)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard segue.identifier == "editCell" else { return }
//        let indexPath = birtdaysTableView.indexPathForSelectedRow!
        
    }
    
    //MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return birthdays.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "personCell", for: indexPath) as! BirthdayTableViewCell
        let birthday = birthdays[indexPath.row]
        cell.photoImage.image = UIImage(named: "person.jpg")
        cell.nameL?.text = birthday.name
        cell.daysToBirthday.text = birthday.dateToBirth
        return cell
    }
    
//    //MARK: - UITableViewDelegate
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100
//    }
    
}

