//
//  BirthdayTableViewCell.swift
//  BirthdayReminder
//
//  Created by roman on 7/31/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class BirthdayTableViewCell: UITableViewCell {

    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var nameL: UILabel!
    @IBOutlet weak var daysToBirthday: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
